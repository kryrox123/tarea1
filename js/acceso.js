var usuarios=[
    {
        userName:"Juan",
        userLastName:"Perez",
        email:"jperez@gmail.com",        
        password:"admin123"
    },
    {
        userName:"Juana",
        userLastName:"Medrano",
        email:"jmedrano@gmail.com",        
        password:"vendedor123"
    }
]

function usuarioRegistrado(userName, password){
    Array.prototype.findBy = function (userName, password) {
        for (var i=0; i<this.length; i++) {
            var object = this[i];
            console.log("Usuario: " + object.userName + " " + "Password: " + object.password)
            if ((object.userName == userName) && (object.password == password)) {
                return true
            }
        }
        return false;
    }
    let contenedor = document.getElementById("contenidoLogin") 
    let html = ""
    if (usuarios.findBy(userName, password)){
        html = `<h1>BIENVENIDO AL SISTEMA</h1>
                <p>Usted ha ingresado como ${userName}</p>`
    }
    else{
        html = `<h1>ERROR EN EL ACCESO AL SISTEMA</h1>
                <p>Revisa tus credenciales.</p>`
    }
    contenedor.innerHTML = html
}

function registrarUsuario(nombre, apellido, email, password){
    let nuevoUsuario = {
        userName: nombre.value,
        userLastName: apellido.value,
        email: email.value,
        password: password.value
    }

    let numUsuario = usuarios.push(nuevoUsuario)
    console.log("Nro de usuario registrado: " + numUsuario)
    alert("Usuario registrado")
    return (numUsuario>0)
}
function validarUsuario(userName, password) {
    Array.prototype.findBy = function (userName, password) {
        for (var i = 0; i < this.length; i++) {
            var object = this[i];
            console.log("Usuario: " + object.userName + " " + "Password: " + object.password);
            if (object.userName == userName && object.password == password) {
                return true;
            }
        }
        return false;
    }

    let contenedor = document.getElementById("contenidoLogin");
    let html = "";

    if (usuarios.findBy(userName, password)) {
        html = `<h1>BIENVENIDO AL SISTEMA</h1>
                <p>Usted ha ingresado como ${userName}</p>`;
    } else {
        html = `<h1>ERROR EN EL ACCESO AL SISTEMA</h1>
                <p>Revisa tus credenciales.</p>`;
    }

    contenedor.innerHTML = html;
}


function registrarUsuario(nombre, apellido, email, password) {
    let nuevoUsuario = {
        userName: nombre,
        userLastName: apellido,
        email: email,
        password: password
    }

    let numUsuario = usuarios.push(nuevoUsuario);
    console.log("Nro de usuario registrado: " + numUsuario);
    alert("Usuario registrado");
    return numUsuario > 0;
}


const registerName = '<?php echo $_REQUEST["Username"]; ?>';
const registerLastName = '<?php echo $_REQUEST["LastName"]; ?>';
const registerEmail = '<?php echo $_REQUEST["Email"]; ?>';
const registerPass = '<?php echo $_REQUEST["Password"]; ?>';


const registroExitoso = registrarUsuario(registerName, registerLastName, registerEmail, registerPass);


if (registroExitoso) {
    console.log("USUARIO REGISTRADO CON ÉXITO");
} else {
    console.log("USUARIO NO REGISTRADO");
}

function registrarUsuario(nombre, apellido, email, password) {
    let nuevoUsuario = {
        userName: nombre.value,
        userLastName: apellido.value,
        email: email.value,
        password: password.value
    }

    let numUsuario = usuarios.push(nuevoUsuario);
    console.log("Nro de usuario registrado: " + numUsuario);

    if (numUsuario > 0) {
        return "USUARIO REGISTRADO CON ÉXITO";
    } else {
        return "USUARIO NO REGISTRADO";
    }
}

function registrarUsuario() {
    const firstName = document.getElementById('first-name').value;
    const lastName = document.getElementById('last-name').value;
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;
    const confirmPassword = document.getElementById('confirm-password').value;

    if (password !== confirmPassword) {
        alert('Las contraseñas no coinciden. Por favor, inténtelo de nuevo.');
        return;
    }

    const user = { firstName, lastName, email };

    const users = JSON.parse(localStorage.getItem('users')) || [];
    users.push(user);

    localStorage.setItem('users', JSON.stringify(users));

    alert('Usuario registrado exitosamente');
    document.getElementById('first-name').value = '';
    document.getElementById('last-name').value = '';
    document.getElementById('email').value = '';
    document.getElementById('password').value = '';
    document.getElementById('confirm-password').value = '';
}

function mostrarUsuarios() {
    const users = JSON.parse(localStorage.getItem('users')) || [];

    if (users.length === 0) {
        alert('No hay usuarios registrados.');
        return;
    }

    const userList = document.createElement('ul');

    users.forEach(user => {
        const listItem = document.createElement('li');
        listItem.textContent = `Nombre: ${user.firstName}, Apellido: ${user.lastName}, Correo: ${user.email}`;
        userList.appendChild(listItem);
    });

    const newWindow = window.open('', '_blank');
    newWindow.document.write('<html><head><title>Usuarios Registrados</title></head><body>');
    newWindow.document.write('<h2>Usuarios Registrados</h2>');
    newWindow.document.write(userList.outerHTML);
    newWindow.document.write('</body></html>');
    newWindow.document.close();
}